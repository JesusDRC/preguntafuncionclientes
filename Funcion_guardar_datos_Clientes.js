function ValidarDatosEstudiante(Cédula,nombres,direccion,telefono,correoElectronico){
            /* Declaración de las Expresiones Regulares*/
const letras= new RegExp('^[A-Za-z ][^\*]+$', 'i');
const Correo= new RegExp('^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$');
const numero= new RegExp('[0-9]');
const lertrasynumeros= new RegExp('/^[A-Za-z0-9\s]+$/g');
            /*Declaracion del mensaje que debera aparecer si no se cumplen los campos establecidos */
var bandera = true;
var mensaje = "Para continuar con el guardado de datos debes completar bien lo siguiente:\n";


if(Cédula.value==""||Cédula.value.length!=10){
    mensaje+= "- Campo de Cédula vacio, porfavor ingresa una cédula y recuerda que solo debe tener 10 caracteres\n";
    bandera=false;
}
else{
    if(!numero.test(Cédula.value)){
    mensaje+= "- Campo de Cédula solo debes de ingresar numeros\n";
    bandera=false;
    }
}


if(nombres.value==""|| nombres.value.length<7 || nombres.value.length>30){
    mensaje+= "- Campo de nombres vacio, porfavor ingresa tus nombres y recuerda que solo debe ingresar un minimo de 7 y un maximo de 30 caracteres\n";
    bandera=false;
}
else{
    if(!letras.test(nombres.value)){
    mensaje+= "- Campo de nombres solo debes de ingresar letras\n";
    bandera=false;
    }
}


if(direccion.value==""|| direccion.value.length<10 || direccion.value.length>40){
    mensaje+= "- Campo de direccion vacio, porfavor ingresa tu direccion y recuerda que solo debe ingresar un minimo de 10 y un maximo de 40 caracteres\n";
    bandera=false;
}
else{
    if(!lertrasynumeros.test(direccion.value)){
    mensaje+= "- Campo de direccion solo debes de ingresar letras y numeros\n";
    bandera=false;
    }
}


if(telefono.value==""||telefono.value.length!=10){
    mensaje+= "- Campo de telefono vacio, porfavor ingresa un numero telefonico valido y recuerda que solo debe tener 10 caracteres\n";
    bandera=false;
}
else{
    if(!numero.test(telefono.value)){
    mensaje+= "- Campo de Cédula solo debes de ingresar numeros\n";
    bandera=false;
    }
}


if(correoElectronico.value=="") {
    mensaje+= "- Campo de correo vacio, ingresa un correo porfavor\n";
    bandera=false;
    }
else{
    if(!Correo.test(correoElectronico.value)){
    mensaje+= "- Campo de correo no valido, ingresa un correo valido ejemplos: Example@gmail.com \n";
    bandera=false;
    }
}
if(bandera==true){
    var Datos=[
        {
        "cedula": Cédula,
        "Nombres": nombres,
        "Direccion": direccion,
        "telefono": telefono,
        "Correo": correoElectronico
        }
    ];
    GuardarDatos(Datos).subscribe(resultado => {
          if(resultado.length== 14){
        alert("Guardado de datos exitoso!!");
        window.location.reload();
          
        }
        else 
          alert("No se guardaron los datos intentalo nuevamente");
          
          window.location.reload();
          
    })
}

if(bandera==false){
    alert(mensaje);
    return bandera;
}
}

//Una vez que se cumplan todos los parametros procederemos a guardar los datos en nuestra base de datos

function GuardarDatos(DatosEstudiante){
    "use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class GuardarDatosEstudiante {
    GuardarDatosEstudiante(DatosEstudiante, respuesta) {
        return __awaiter(this, void 0, void 0, function* () {
            const cedula = DatosEstudiante.params.cedula;
            const nombres = DatosEstudiante.params.nombres;
            const direccion = DatosEstudiante.params.direccion;
            const telefono = DatosEstudiante.params.telefono;
            const correo = DatosEstudiante.params.correoElectronico;
            yield database_1.default.query('INSERT INTO Clientes set ?', [cedula, nombres, direccion, telefono, correo]);
            return respuesta.json('registro exitoso');
        });
    }
}

exports.guardarDatosEstudiante = new GuardarDatosEstudiante;
exports.default = exports.guardarDatosEstudiante;


}